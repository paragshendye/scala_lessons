println("Define a function with variable number of arguments")
def printReport(names: String*): Unit = {
  println(s"Donut Report= ${names.mkString(", ")}")
}
println("calling printReport function with variable parameters")
println(printReport("Double Trouble", "Black Foresst", "Vanilla Delight"))
println(printReport("Double Trouble"))

println("we can also pass list in function")
val listOfDonuts: List[String] = List("Double Trouble", "Some random", "burger donut ")
printReport(listOfDonuts: _*)

println(s"How to pass sequence to a function")
val seDonuts: Seq[String] = Seq("Chocolate Donut", "Vanilla Donut")
printReport(seDonuts: _*)
println("similarly array can be passed as a function to printReport function")

//Working with classes
println("How to create  a solution")

class DonutCostCalculator {
  val totalCost = 100

  def minusDiscount(discount: Double): Double = {
    totalCost - discount
  }

  println("How to define a function with a symbol as its name")

  def !(discount: Double): Double = {
    totalCost - discount
  }


}

println("Instantiating a class")
val donutCostCalculator = new DonutCostCalculator()
println(s"Calling minusDiscout() function: ${donutCostCalculator.minusDiscount(10.5)}")
println(s"calling a function with symbol as name: ${donutCostCalculator ! (10.5)}")

println("How to define function with curried parameter groups")

def totalCost(donutType: String)(quantity: Double)(discount: Double): Double = {
  val totalCost = 2.50 * quantity
  totalCost - (totalCost * discount)

}

println(s"Total Cost = ${totalCost("glazed donut")(10)(0.05)}")

println("How to create a partially applied function from a function with curried parameter groups")
val totalCostForGlazedDonuts = totalCost("Glazed Donut") _

/*Note that the return type of the partially applied function totalCostForGlazedDonuts is Int => Double => Double. The first Int is for our quantity parameter, the Double is for discount parameter and the last Double the return type of the function.
 In short, the partially applied function creates a chain of functions.*/

/*Calling the partially applied function totalCostForGlazedDonuts is no different than how you've called the curried function totalCost in Step 2 by enclosing each parameter within ().



However, you do not need to fill in the first parameter for the donutType String parameter as you have already
pre-filled it with the Glazed Donut String*/

println(s"TotalCost for Glazed Donuts = ${totalCostForGlazedDonuts(10)(0.1)}")

println("How to define a high order fucntion which take in another function as parameter")

def totalCostWithDiscountFunctionParameter(donutType: String)(quantity: Int)(f: Double => Double): Double = {
  println(s"Calculating total cost for $quantity $donutType")
  val totalCost = 2.50 * quantity
  f(totalCost)
}

println("How to call Higher order function")
val totalCostOfNDonuts = totalCostWithDiscountFunctionParameter("Glazed")(10) {
  totalCost =>
    val discount = 2
    totalCost - discount
}
println(s"totalCost of n Donuts ${totalCostOfNDonuts}")

/*A better approach to Step 3 is to pass-through a common discount function which would encapsulate the discount logic instead of providing an anonymous function.



To this end, let's create a function named applyDiscount as follows:*/

def applyDiscount(totalCost: Double): Double = {
  val discount = 2
  totalCost - discount

}

println(s"Total cost of N donuts with discount function=${totalCostWithDiscountFunctionParameter("Glazed")(5)(applyDiscount(_))}")

//HIGHER ORDER FUNCTIONS
//functions that take in other functions at input parameters

def sumInts(a: Int, b: Int): Int =
  if (a > b) 0 else a + sumInts(a + 1, b)

println(sumInts(2, 6))

// this can be mathematically expressed as
//submission(i) from a to b

//lets generalize for other such cases
// f(x) = x*x*x

def sumfuncs(f: Int => Int, a: Int, b: Int): Int = {
  if (a > b) 0
  else f(a) + sumfuncs(f, a + 1, b)

}

def cube(x: Int): Int = x * x * x
//now f can be any function of our choice which gives int as a return type
def sumCubes(a: Int, b: Int) = sumfuncs(cube, a, b)


println(s"cubes: ${sumCubes(2, 4)}")

// but here we have defined the auxillary functions used
// this can be replaced with "anonymous functions"

// ANONYMOUS FUNCTIONS
// these functions can always be expressed as regular functions

def sumAnaonymousCubes(a: Int, b: Int) = sumfuncs(x => x * x * x, a, b)

println(s"sumAnonymouscubes: ${sumAnaonymousCubes(2, 4)}")


//Write a tail-recursive version of sum
def sumTailrecusrsive(f: Int => Int, a: Int, b: Int): Int = {
  def loop(a: Int, acc: Int): Int = {
    if (a > b) acc
    else loop(a + 1,f(a)+ acc)
  }
    loop(a, 0)


  }

println(sumTailrecusrsive(x=>x*x,3,5))


//CURRYING FUNCTIONS

// def sumInts(a:Int, b:Int)= sumfuncs(x=>x,a,b)

//above we see repition of a and b which are passed unchanged

def sum(f:Int=>Int):(Int,Int)=>Int={
  def sumF(a:Int,b:Int):Int =
    if(a>b)0
    else f(a) + sumF(a+1,b)

  sumF


}
println(s"currying: ${sum(x=>x*x*x)(1,4)}")

//even better way of currying can be
def sumbetter(f:Int=>Int)(a:Int,b:Int):Int ={
  if(a>b)0 else f(a)+sumbetter(f)(a+1,b)
}
println(s"curryingSumBetter: ${sumbetter(x=>x*x*x)(1,4)}")


//Write a product function on similar lines

def productbetter(f:Int=>Int)(a:Int,b:Int):Int = {
  if (a>b) 1 else f(a)*productbetter(f)(a+1,b)

}
println(s"curryingProductbetter: ${productbetter(x=>x)(1,4)}")

// Write factorial in terms of product

def factorialCurry(n:Int)= productbetter(x=>x)(1,n)

println(s"factorialCurry: ${factorialCurry(4)}")

// Making another HIGHER ORDER FUNCTION from "SCALA for the impatient"
def totalCostWithDiscountFunctionParameter2(donutType:String)(quantity:Int)(f:Double=>Double):Double={
  val totalCost = 2.5*quantity

  f(totalCost)

}

def applyDiscount2(totalCost:Double):Double={

  val discount = 2
  totalCost - discount

}

println(s"totalCost for 10 donuts of type Glazed is ${totalCostWithDiscountFunctionParameter2("Glazed")(10)(applyDiscount2(_))}")

//Define a function to loop through each Tuple3 elements of
//list and calculate TotalCost

val listOrders = List(("Glazed donut",5,2.50),("Double Trouble",10,3.0))

def placeOrder(orders:List[(String,Int,Double)]):Double={
  var totalCost:Double = 0.0

  orders.foreach{order=>
    val costofItem = order._2*order._3
    println(s"cost of ${order._2} ${order._1}= $costofItem")
    totalCost+=costofItem
  }
  totalCost


}

println(s"Total cost of order: ${placeOrder(listOrders)}")

//How to call andThen function

val totalCost =10

val applyDiscountFunction= (amount:Double)=>{
  println("apply discount function")
  val discount =2
  amount - discount
}

println(s"total cost of donuts with discount: ${applyDiscountFunction(totalCost)}")

val applyTaxFunction= (amount:Double)=>{
  println("apply tax function")
  val tax = 1
  amount + tax
}

println(s"Total cost of donuts = ${(applyDiscountFunction andThen applyTaxFunction)(totalCost)}")

//Using compose function on val type functions
/*Calling compose will take the result from the second function and
pass it as input parameter to the first function.
Let's use the compose semantics to apply tax first and
afterwards apply discount to the totalCost figure as shown below.
*/

println(s"Total cost= ${(applyDiscountFunction compose applyTaxFunction)(totalCost)}")


//Ordering using andThen: f(x) andThen g(x) = g(f(x))
//Ordering using compose: f(x) compose g(x) = f(g(x))

class simplegreeter{
  val greeting = "Hello World!"
  def greet() = println(greeting)


}

val g = new simplegreeter
g.greet()

class greetrepeat(greeting:String, count: Int){
  def this(greeting:String)=this(greeting,1)

  def greet()={
    for(i<- 1 to count){
      println(greeting)
    }
  }
}//class is itself primary constructor
// this represents another constructor
val g1 = new greetrepeat("hello",3)
g1.greet()
val g2 = new greetrepeat("hi")
g2.greet()

