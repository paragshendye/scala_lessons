//Using String interpolation to print a variable
val favoriteDonut : String = "Double-Trouble"
println(s"my favorite donut is: $favoriteDonut")

//Using String interpolation on object properties
println("\nStep 2: Using String interpolation on object properties")
case class Donut(name: String, tasteLevel: String)
val favoriteDonut2: Donut = Donut("Black Decadence","Very tasty")
println(s"My favorite Donut is: ${favoriteDonut2.name},${favoriteDonut2.tasteLevel}")

//Using String interpolation to evaluate expressions

println("\nStep 3: Using String interpolation to evaluate expressions")
val qtyDonuts : Int = 10
println(s"Do we have 10 donuts = ${qtyDonuts==10}")

//A simple for loop from 1 to 5 inclusive

println("Step 1: A simple for loop from 1 to 5 inclusive")
for (numberOfDomuts <- 1.to(5)){
  println(s"Number of donuts to buy: $numberOfDomuts")
}

//A simple for loop from 1 to 5 exclusive
for (numberOfDomuts <- 1.until(5)){
  println(s"Number of donuts to buy: $numberOfDomuts")
}

//Filter values using if conditions in for loop

println("\nStep 3: Filter values using if conditions in for loop")
val donutIngredients = List("flour", "sugar", "egg yolks", "syrup", "flavouring")

for(ingredient <- donutIngredients if ingredient == "sugar"){
  println(s"Found sweetning ingredient = $ingredient")
}
//Filter values using if conditions in for loop and return the result back using the yield keyword

val sweetningIngredients = for {ingredient <- donutIngredients if(ingredient=="sugar" || ingredient=="syrup" )} yield ingredient
println(s"Sweetning ingredients= $sweetningIngredients")

// Using for comprehension to loop through 2-Dimensional array
val twoDimensionalArray = Array.ofDim[String](2,2)
twoDimensionalArray(0)(0)="flour"
twoDimensionalArray(0)(1)="sugar"
twoDimensionalArray(1)(0)="syrup"
twoDimensionalArray(1)(1)="raisins"

for{x<- 0.until(2)
    y<- 0.until(2)}println(s"Donut ingerdient at index ${(x,y)}=${twoDimensionalArray(x)(y)}")

// Create a simple numeric range from 1 to 5 inclusive

println("Step 1: Create a simple numeric range from 1 to 5 inclusive")
val from1_to_5 = 1.to(5)
println(s"range from 1 to 5= $from1_to_5")

val sequenceFrom1To5 = (1 to 5).toSeq
println(s"Range to sequence = ${sequenceFrom1To5.mkString(" ")}")

//1. Pattern matching 101 - a very basic example

val DonutType = "good Donut"

DonutType match {
  case "Glazed Donut" => println("very tasty")
  case "Plain Donut" => println("no")
  case _ => println("no")
}

val tasteanother = DonutType match{
  case donut if (donut.contains("Glazed")||donut.contains("sugar")) => "very tasty"
  case "Plain Donut"=> "tasty"
  case _=> "tasty"

}
println(s"taste level of $DonutType=$tasteanother")

//How to use Option and Some - a basic example

val glazedDonutTaste: Option[String] = Some("Very Tasty")
println(s"Glazed Donut taste = ${glazedDonutTaste.get}")


val glazedDonutName: Option[String] = None
println(s"Glazed Donut name = ${glazedDonutName.getOrElse(" ")}")

glazedDonutName match {
  case Some(name) => println(s"Received donut name = $name")
  case None       => println(s"No donut name was found!")
}

println("How to define option in function parameter")

def calculateDonutCost(donutName:String, quantity: Int, couponCode:Option[String]):Double={
  println(s"calculating cost of ${donutName},quantity = ${quantity}")

  couponCode match {
    case Some(coupon)=>
      val discount=0.1
      val totalCost = 2.50*quantity*(1-discount)
      totalCost

    case None=>2.50*quantity
  }
}

val trycalculatecost = calculateDonutCost("Double Trouble",5,Some("somecode"))
println(trycalculatecost)

println("Use the map function to extract a valid option value")

val likeableDonut: Option[String]=Some("Glazed Donut")
likeableDonut.map(d=>println(s"likeable Donut=$d"))

println("Define a function which returns an Option of type String")
def dailyCouponcode():Option[String]={
  val couponFromDB = "Coupon 1234"
  Option(couponFromDB).filter(_.nonEmpty)


}

val todayCoupon:Option[String]=dailyCouponcode()
println(s"${todayCoupon.getOrElse("Not found")}")

println("Call fucntion using option return type using pattern matching")
dailyCouponcode() match {
  case Some(coupon)=>println(s"today's coupon=$coupon")
  case None=>println("no coupon available")
}

println(calculateDonutCost("glazed",10,dailyCouponcode()))

println(s"\nTip - 1: Call function with Option return type using fold")

val todayCouponUsingFold: String =dailyCouponcode().fold("No Coupon Code")(couponCode =>couponCode)
println(s"Today's coupon code= $todayCouponUsingFold")

println("\nStep 1: Review how to define a generic typed function which will specify the type of its parameter")

def applyDiscount[T](discount:T){
  discount match {
    case d: String=>
      println(s"look up discount in database for $d")
    case d: Double=>
      println(s"$d discount has been applied")

    case _ =>
      println("unsupported discount type")
  }

}

println("\nStep 2: Review how to call a function which has typed parameters")
applyDiscount[String]("Coupon 123")
applyDiscount[Double](5.0)

//replaceAll method

val speech=
  """Four score and
    |seven years ago
    |our fathers
  """.stripMargin.replaceAll("\n"," ")

println(speech)

val UPPER = "hello world".map(c=>c.toUpper)
val UPPERNEW = "hello world".map(_.toUpper)

//REGEX

val numPattern = "[0-9]+".r

val address = "123 main street suite 101"

val match1 = numPattern.findFirstIn(address)
val match2 = numPattern.findAllIn(address)
val match3 = numPattern.findAllMatchIn(address).toArray
match2.foreach(println)

val result = match1.getOrElse("no match")

match1 match {
  case Some(s) => println(s"found: ${s}")
  case _ =>
}

val newaddress = numPattern.replaceAllIn(address,"x")

val pattern  = "([0-9]+)([A-Za-z]+)".r

val match4 = pattern.findAllMatchIn(address)

/* List Methods */
val somelist = List()

val emptylist = Nil

val someotherlist = ("Cool","tools","rule")

val thrill = "Will"::"fill"::"until"::Nil

println(thrill(2))

println(thrill.count(s=>s.length ==4))

thrill.drop(2)

thrill.exists(s=>s=="until")

thrill.filter(s=>s.length==4)

thrill.foreach(print)

thrill.map(s=>s+"y")

thrill.head
thrill.tail