package week2

object Intset {



}

abstract class Intset{
  def incl(x:Int):Intset
  def contains(x:Int):Boolean
}

class Empty extends Intset{
  override def contains(x: Int): Boolean = false

  override def incl(x: Int): Intset = new NonEmpty(x, new Empty,new Empty)

}

