package week2

object rationals extends App{
  val x = new Rational(2,3)
  val y = new Rational(4,5)
  val z = new Rational(3,8)
  //val strange =  new Rational(1,0)
  //println(strange.addRational(strange))

  println(x.addRational(y))
  println(x.subRational(y))
  println(x.neg())
  println(x.subRational(y).subRational(z))

  println(x.less(y))
  println(x.max(y))


}
