package week2

class Rational(x:Int,y:Int) {
  require(y!=0, "denominator should be non-zero")

  // a second constructor of the class can be defined in the class itself
  def this(x:Int)=this(x,1)


  private def gcd(a:Int,b:Int):Int = if (b==0) a else gcd(b,a%b)
  private val g =gcd(x,y)

  def numer = x/g
  def denom =y/g

  def addRational(other: Rational):Rational={
    new Rational(numer*other.denom+denom*other.numer,denom*other.denom)
  }

  def subRational(other:Rational):Rational=addRational(other.neg())

  override def toString: String = numer+"/"+denom

  def neg():Rational=new Rational(-numer,denom)

  def less(other:Rational)=numer*other.denom<denom*other.numer

  def max(other:Rational)=if (this.less(other)) other else this



}
